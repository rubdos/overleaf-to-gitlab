---
variables: &variables
  CARGO_HOME: $CI_PROJECT_DIR/cargo

stages:
  - build
  - test
  - deploy

.rust: &rust
  cache: &rust-cache
    paths:
      - cargo/
      - target/
      - $CI_PROJECT_DIR/cargo
  before_script:
    - cargo --version
    - rustc --version

.rust-stable: &rust-stable
  <<: *rust
  image: rust
  cache:
    <<: *rust-cache
    key: rust-stable

.rust-nightly: &rust-nightly
  <<: *rust
  image: rustlang/rust:nightly
  cache:
    <<: *rust-cache
    key: rust-nightly

.kaniko: &kaniko
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  cache:
    key: docker
  stage: build
  before_script:
    - touch /kaniko/.docker/config.json
    - echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
    - echo "Working around https://github.com/GoogleContainerTools/kaniko/issues/595"
    - rm -f .dockerignore

build:stable:
  <<: *rust-stable
  stage: build
  script:
    - cargo build --all-features
    - cargo build --release --all-features

build:nightly:
  <<: *rust-nightly
  stage: build
  script:
    - cargo build --all-features
    - cargo build --release --all-features

build:nightly:docs:
  <<: *rust-nightly
  stage: build
  script:
    - cargo rustdoc --all-features
  artifacts:
    paths:
      - target/doc

build:docker:
  <<: *kaniko
  script:
    - /kaniko/executor --context $CI_PROJECT_DIR --dockerfile $CI_PROJECT_DIR/Dockerfile --destination $CI_REGISTRY_IMAGE:$CI_PIPELINE_IID --cache=true

test:stable:
  <<: *rust-stable
  stage: test
  script:
    - cargo test --all-features
    - cargo test --release --all-features

test:nightly:
  <<: *rust-nightly
  stage: test
  script:
    - cargo test --all-features
    - cargo test --release --all-features

coverage:nightly:
  image: xd009642/tarpaulin:develop-nightly
  stage: test
  script:
    - cargo tarpaulin --release -v -t 360 --exclude-files cargo/*

deploy:docker:
  <<: *kaniko
  stage: deploy
  only:
    - master
  script:
    - /kaniko/executor --context $CI_PROJECT_DIR --dockerfile $CI_PROJECT_DIR/Dockerfile --destination $CI_REGISTRY_IMAGE:latest --cache=true
