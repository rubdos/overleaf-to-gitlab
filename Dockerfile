FROM rust:latest AS build-image

WORKDIR /src

COPY Cargo.toml .
RUN mkdir src
RUN touch src/lib.rs

RUN mkdir .cargo

RUN cargo vendor --manifest-path Cargo.toml > .cargo/config

# Cache depedencies build
RUN cargo build --release

# Load the actual source code
COPY . .

RUN cargo build --release

FROM debian:latest as runner

COPY --from=build-image /src/target/release/overleaf-to-gitlab /usr/local/bin/overleaf-to-gitlab
COPY --from=build-image /src/target/release/gitlab-to-overleaf /usr/local/bin/gitlab-to-overleaf

CMD ["overleaf-to-gitlab"]
