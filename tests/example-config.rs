use overleaf_to_gitlab::config::Config;

#[test]
fn parse_example_config() {
    let _cfg: Config = serde_yaml::from_str(include_str!("../config-example.yaml")).unwrap();
}
