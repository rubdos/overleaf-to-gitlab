lazy_static::lazy_static! {
    static ref ENV_MUTEX: std::sync::Mutex<()> = {
        std::sync::Mutex::new(())
    };
}

pub(crate) struct MockEnv {
    env: Vec<&'static str>,
    _lock: std::sync::MutexGuard<'static, ()>,
}

impl Default for MockEnv {
    fn default() -> Self {
        MockEnv {
            env: Vec::new(),
            _lock: ENV_MUTEX.lock().unwrap(),
        }
    }
}

impl MockEnv {
    pub fn set_var(&mut self, key: &'static str, val: &'static str) {
        std::env::set_var(key, val);
        self.env.push(key);
    }
}

impl Drop for MockEnv {
    fn drop(&mut self) {
        for key in self.env.drain(..) {
            std::env::remove_var(key);
        }
    }
}

#[test]
fn check_mock_env() {
    let key = "foobar";
    assert!(std::env::var(key).is_err());

    let mut mock = MockEnv::default();
    mock.set_var(key, "val");
    assert_eq!(std::env::var(key), Ok("val".into()));

    drop(mock);
    assert!(std::env::var(key).is_err());
}
