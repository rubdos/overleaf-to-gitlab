use overleaf_to_gitlab::*;

fn main() -> Result<(), anyhow::Error> {
    dotenv::dotenv().ok();
    let cfg = config::Config::read(".overleaf-to-gitlab.yaml")?;
    let _gitlab = overleaf_to_gitlab::gitlab::from_config(&cfg)?;

    Ok(())
}
