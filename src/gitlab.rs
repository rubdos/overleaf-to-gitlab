use gitlab::Gitlab;

use thiserror::Error;

#[derive(Error, Debug)]
pub enum GitlabConfigError {
    #[error("Gitlab initialization error")]
    GitlabError(#[from] gitlab::GitlabError),
    #[error("Environment error, please run in a Gitlab CI environment")]
    EnvironmentError(#[from] std::env::VarError),
    #[error("CI_SERVER_PROTOCOL should be 'https' or 'http'")]
    ProtocolError,
}

pub fn from_config(_cfg: &crate::config::Config) -> Result<Gitlab, GitlabConfigError> {
    // XXX: Currently, no gitlab settings are sourced from config.
    //      We assume we're running inside CI, so everything should be in the environment.
    //      We already pass in the config for future changes to that idea.

    let proto = std::env::var("CI_SERVER_PROTOCOL")?;
    let host = std::env::var("CI_SERVER_HOST")?;
    let job_token = std::env::var("CI_JOB_TOKEN")?;
    match &proto as &str {
        "https" => Ok(Gitlab::new(host, job_token)?),
        "http" => Ok(Gitlab::new_insecure(host, job_token)?),
        _ => Err(GitlabConfigError::ProtocolError),
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::mock_environment::MockEnv;

    use anyhow::Context;

    fn setup_env() -> MockEnv {
        let mut env = MockEnv::default();
        env.set_var("CI_SERVER_PROTOCOL", "https");
        env.set_var("CI_SERVER_HOST", "gitlab.com");
        env.set_var("CI_JOB_TOKEN", "xyz");

        env
    }

    #[test]
    fn fail_without_env() {
        assert!(crate::gitlab::from_config(&crate::config::Config::default()).is_err());

        let _env = setup_env();

        match crate::gitlab::from_config(&crate::config::Config::default()) {
            Ok(_) => (),
            Err(GitlabConfigError::GitlabError(_)) => (), // Unconnectable with the fake CI token
            Err(e) => Err(e)
                .with_context(|| "Gitlab should be able to construct when environment is set.")
                .unwrap(),
        }
    }
}
