use anyhow::Context;

fn master() -> String {
    "master".into()
}

fn overleaf() -> String {
    "overleaf".into()
}

#[derive(Default, serde::Deserialize)]
pub struct Config {
    pub sources: Vec<OverleafSource>,
    #[serde(default = "master")]
    pub target_branch: String,
    #[serde(default = "overleaf")]
    pub overleaf_branch: String,
}

#[derive(serde::Deserialize)]
pub struct OverleafSource {
    pub project_id: String,
    #[serde(default)]
    pub target_directory: std::path::PathBuf,
    pub credentials: Option<Credentials>,
}

#[derive(serde::Deserialize)]
#[serde(untagged)]
pub enum Credentials {
    Environment {
        username_variable: String,
        password_variable: String,
    },
    Direct {
        username: String,
        password: String,
    },
    None,
}

impl Config {
    pub fn read(filename: impl AsRef<std::path::Path>) -> Result<Self, anyhow::Error> {
        let f = std::fs::File::open(filename.as_ref())
            .with_context(|| format!("Failed to open file {:?}", filename.as_ref()))?;
        Ok(serde_yaml::from_reader(f)?)
    }
}
