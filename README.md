For those who like merge requests and continuous integration,
and dislike Overleaf commiting every character change.

# Overleaf to Gitlab

Overleaf to Gitlab is a tool that integrates Overleaf documents with [Gitlab](https://gitlab.com) and [Gitlab CI](https://docs.gitlab.com/ee/ci/),
by creating and maintaining a branch and merge request on your repository.
It is meant to be used as a tool in your Gitlab CI.

# Getting started

Add this to your `.gitlab-ci.yml`:
```yaml
import_overleaf:
  image: registry.gitlab.com/rubdos/overleaf-to-gitlab:latest
  only:
    - schedules
  script:
    - overleaf-to-gitlab

# Best run this *after* building stage
export_overleaf:
  image: registry.gitlab.com/rubdos/overleaf-to-gitlab:latest
  # stage: deploy
  allow_failure: true
  script:
    - gitlab-to-overleaf
```

... and add a configuration file `.overleaf-to-gitlab.yaml`:
```yaml
target_branch: master # optional
overleaf_branch: overleaf # optional
sources:
  - project_id: 5e49d1650882a30001ca81ce
    target_directory: overleaf/sources/go/in/here
    credentials:
      username_variable: ETRO_OVERLEAF_USERNAME
      password_variable: ETRO_OVERLEAF_PASSWORD
```

... and configure a [scheduled pipeline](https://docs.gitlab.com/ee/ci/pipelines/schedules.html).
